package fr.irit.melodi.sparql.constants;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Constants {
	public static final Set<String> SUPPORTED_EXTENSIONS = Stream.of("sparql", "ttl")
	         .collect(Collectors.toCollection(HashSet::new));
}
