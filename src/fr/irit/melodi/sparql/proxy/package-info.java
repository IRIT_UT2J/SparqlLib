/**
 * 
 */
/**
 * This package gathers the classes related to the interrogation of remote KB with SPARQL queries.
 *
 */
package fr.irit.melodi.sparql.proxy;