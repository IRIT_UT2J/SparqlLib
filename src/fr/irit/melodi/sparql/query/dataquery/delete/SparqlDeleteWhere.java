package fr.irit.melodi.sparql.query.dataquery.delete;

import java.util.Map;
import java.util.Set;

import fr.irit.melodi.sparql.query.dataquery.SparqlAbstractDataQuery;

/**
 * SPARQL DELETE query, with a WHERE clause
 */
public class SparqlDeleteWhere extends SparqlAbstractDataQuery
{
	public SparqlDeleteWhere(Set<Map.Entry<String, String>> prefix, String data)
	{
		super(prefix, data);
		this.keyword = "DELETE WHERE";
	}
}
