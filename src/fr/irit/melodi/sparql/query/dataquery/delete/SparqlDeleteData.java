package fr.irit.melodi.sparql.query.dataquery.delete;

import java.util.Map;
import java.util.Set;

import fr.irit.melodi.sparql.query.dataquery.SparqlAbstractDataQuery;

/**
 * SPARQL DELETE query with no WHERE clause
 */
public class SparqlDeleteData extends SparqlAbstractDataQuery
{
	public SparqlDeleteData(Set<Map.Entry<String, String>> prefix, String data)
	{
		super(prefix, data);
		this.keyword = "DELETE DATA";
	}
}
