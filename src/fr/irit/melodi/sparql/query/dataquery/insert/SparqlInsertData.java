package fr.irit.melodi.sparql.query.dataquery.insert;

import java.util.Map;
import java.util.Set;

import fr.irit.melodi.sparql.query.dataquery.SparqlAbstractDataQuery;

/**
 * SPARQL INSERT query, with no WHERE clause
 */
public class SparqlInsertData extends SparqlAbstractDataQuery
{
	public SparqlInsertData(Set<Map.Entry<String, String>> prefix, String data)
	{
		super(prefix, data);
		this.keyword = "INSERT DATA";
	}
	
	public void setKeyWords(String keyword)
	{
		this.keyword = keyword;
	}
}
