package fr.irit.melodi.sparql.query.ask;

import fr.irit.melodi.sparql.query.SparqlQuery;
import java.util.Map.Entry;
import java.util.Set;

/**
 * Representation of a SPARQL ASK query. It only contains a WHERE clause.
 */
public class SparqlAsk extends SparqlQuery
{
	public SparqlAsk(Set<Entry<String, String>> prefix, String data)
	{
		super(prefix, "", data);
	}
	
	public String toString()
	{
		return this.formatPrefixes()+" ASK{"+this.getWhere()+"}";
	}

}
